package com.note.tool;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class LoopedStreams {
    private PipedOutputStream pipedOS = new PipedOutputStream();

    private boolean keepRunning = true;

    private ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream() {
        public void close() {
            LoopedStreams.this.keepRunning = false;
            try {
                super.close();
                LoopedStreams.this.pipedOS.close();
            } catch (IOException e) {
                System.exit(1);
            }
        }
    };

    private PipedInputStream pipedIS = new PipedInputStream() {
        public void close() {
            LoopedStreams.this.keepRunning = false;
            try {
                super.close();
            } catch (IOException e) {
                System.exit(1);
            }
        }
    };

    public LoopedStreams() throws IOException {
        this.pipedOS.connect(this.pipedIS);
        startByteArrayReaderThread();
    }

    public InputStream getInputStream() {
        return this.pipedIS;
    }

    public OutputStream getOutputStream() {
        return this.byteArrayOS;
    }

    private void startByteArrayReaderThread() {
        (new Thread(new Runnable() {
            public void run() {
                while (LoopedStreams.this.keepRunning) {
                    if (LoopedStreams.this.byteArrayOS.size() > 0) {
                        byte[] buffer = null;
                        synchronized (LoopedStreams.this.byteArrayOS) {
                            buffer = LoopedStreams.this.byteArrayOS.toByteArray();
                            LoopedStreams.this.byteArrayOS.reset();
                        }
                        try {
                            LoopedStreams.this.pipedOS.write(buffer, 0, buffer.length);
                        } catch (IOException e) {
                            System.exit(1);
                        }
                        continue;
                    }
                    try {
                        Thread.sleep(1000L);
                    } catch (InterruptedException interruptedException) {}
                }
            }
        })).start();
    }
}
