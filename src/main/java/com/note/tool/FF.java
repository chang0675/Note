package com.note.tool;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class FF extends JFrame implements ItemListener, ActionListener {
    private static final long serialVersionUID = 1L;

    static String bit = "64";

    static Font font;

    static Font font2;

    static Color color;

    static Color color2;

    static Color color3;

    static int width;

    static int height;

    static String begin;

    static String end;

    static String bat;

    static String out;

    static String name;

    static boolean top;

    GridBagLayout layout;

    GridBagConstraints gbc;

    JMenuBar jmb;

    JFileChooser fc;

    static File file;

    static JPanel toolPane;

    static JCheckBox cb;

    static JCheckBox ck;

    JTextField jtf;

    static JTextField text1;

    static JTextField text2;

    static JTextField text3;

    static JTextField text4;

    static JTextField text5;

    static JTextArea textarea;

    static {
        try {
            ReadWriter.read();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String[] m1Items = new String[]{"font", "theme", "clear"};

    String[] m2Items = new String[]{"version", "help"};

    FF() {
        setTitle("B取");
        setSize(width, height);
        setLocationRelativeTo((Component) null);
        setAlwaysOnTop(top);
        this.jmb = new JMenuBar();
        this.jtf = new JTextField();
        this.jtf.setFont(new Font("Microsoft YaHei UI", 0, 16));
        this.jtf.setEditable(false);
        setJMenuBar(this.jmb);
        add(this.jtf, "South");
        JMenu m1 = makeJMenu("setting", 'S');
        makeItem(this.m1Items, m1);
        JMenu m2 = makeJMenu("about", 'A');
        makeItem(this.m2Items, m2);
        toolPane = new JPanel();
        this.layout = new GridBagLayout();
        toolPane.setLayout(this.layout);
        this.gbc = new GridBagConstraints();
        this.gbc.fill = 1;
        this.gbc.insets = new Insets(10, 10, 10, 10);
        this.gbc.gridx = 0;
        this.gbc.gridy = 0;
        this.gbc.weightx = 5.0D;
        this.gbc.weighty = 1.0D;
        this.gbc.gridwidth = 1;
        this.gbc.gridheight = 1;
        makeButton("begin", toolPane, this.layout, this.gbc).setToolTipText("select only, no copy and paste");
        this.gbc.gridx = 1;
        this.gbc.gridy = 0;
        this.gbc.weightx = 5.0D;
        this.gbc.weighty = 1.0D;
        this.gbc.gridwidth = 1;
        this.gbc.gridheight = 1;
        makeButton("end", toolPane, this.layout, this.gbc).setToolTipText("on folder 16 | 64 | 80 | 128");
        this.gbc.gridx = 2;
        this.gbc.gridy = 0;
        this.gbc.weightx = 5.0D;
        this.gbc.weighty = 1.0D;
        this.gbc.gridwidth = 1;
        this.gbc.gridheight = 1;
        makeButton("bat", toolPane, this.layout, this.gbc).setToolTipText("where .bat file is");
        this.gbc.gridx = 3;
        this.gbc.gridy = 0;
        this.gbc.weightx = 1.0D;
        this.gbc.weighty = 1.0D;
        this.gbc.gridwidth = 1;
        this.gbc.gridheight = 1;
        ck = makeCheckBox("top", this.layout, this.gbc);
        this.gbc.gridx = 4;
        this.gbc.gridy = 0;
        this.gbc.weightx = 15.0D;
        this.gbc.weighty = 1.0D;
        this.gbc.gridwidth = 3;
        this.gbc.gridheight = 1;
        makeButton("output", toolPane, this.layout, this.gbc);
        this.gbc.gridx = 0;
        this.gbc.gridy = 1;
        this.gbc.weightx = 1.0D;
        this.gbc.weighty = 1.0D;
        this.gbc.gridwidth = 4;
        this.gbc.gridheight = 1;
        makeLabel("select begin folder on " + bit + " such as 'E:\\data\\92191094\\c_317871633\\" + bit + "\\'", toolPane, this.layout, this.gbc);
        this.gbc.gridx = 0;
        this.gbc.gridy = 2;
        this.gbc.weightx = 3.0D;
        this.gbc.weighty = 4.0D;
        this.gbc.gridwidth = 3;
        this.gbc.gridheight = 1;
        text1 = makeTextField(begin, toolPane, this.layout, this.gbc);
        this.gbc.insets.set(10, -12, 10, 10);
        this.gbc.gridx = 3;
        this.gbc.gridy = 2;
        this.gbc.weightx = 1.0D;
        this.gbc.weighty = 4.0D;
        this.gbc.gridwidth = 1;
        this.gbc.gridheight = 1;
        JButton sel1 = makeButton("...", toolPane, this.layout, this.gbc);
        this.gbc.insets.set(10, 10, 10, 10);
        this.gbc.gridx = 0;
        this.gbc.gridy = 3;
        this.gbc.weightx = 4.0D;
        this.gbc.weighty = 1.0D;
        this.gbc.gridwidth = 4;
        this.gbc.gridheight = 1;
        makeLabel("select end folder on " + bit + " such as 'E:\\data\\92191094\\c_320577177\\" + bit + "\\'", toolPane, this.layout, this.gbc);
        this.gbc.gridx = 0;
        this.gbc.gridy = 4;
        this.gbc.weightx = 3.0D;
        this.gbc.weighty = 4.0D;
        this.gbc.gridwidth = 3;
        this.gbc.gridheight = 1;
        text2 = makeTextField(end, toolPane, this.layout, this.gbc);
        this.gbc.insets.set(10, -12, 10, 10);
        this.gbc.gridx = 3;
        this.gbc.gridy = 4;
        this.gbc.weightx = 1.0D;
        this.gbc.weighty = 4.0D;
        this.gbc.gridwidth = 1;
        this.gbc.gridheight = 1;
        JButton sel2 = makeButton("...", toolPane, this.layout, this.gbc);
        this.gbc.insets.set(10, 10, 10, 10);
        this.gbc.gridx = 0;
        this.gbc.gridy = 5;
        this.gbc.weightx = 4.0D;
        this.gbc.weighty = 1.0D;
        this.gbc.gridwidth = 4;
        this.gbc.gridheight = 1;
        makeLabel("select bat folder such as 'E:\\data\\92191094\\c_235888729\\64\\'", toolPane, this.layout, this.gbc);
        this.gbc.gridx = 0;
        this.gbc.gridy = 6;
        this.gbc.weightx = 3.0D;
        this.gbc.weighty = 4.0D;
        this.gbc.gridwidth = 3;
        this.gbc.gridheight = 1;
        text3 = makeTextField(bat, toolPane, this.layout, this.gbc);
        this.gbc.insets.set(10, -12, 10, 10);
        this.gbc.gridx = 3;
        this.gbc.gridy = 6;
        this.gbc.weightx = 1.0D;
        this.gbc.weighty = 4.0D;
        this.gbc.gridwidth = 1;
        this.gbc.gridheight = 1;
        JButton sel3 = makeButton("...", toolPane, this.layout, this.gbc);
        this.gbc.insets.set(10, 10, 10, 10);
        this.gbc.gridx = 0;
        this.gbc.gridy = 7;
        this.gbc.weightx = 4.0D;
        this.gbc.weighty = 1.0D;
        this.gbc.gridwidth = 4;
        this.gbc.gridheight = 1;
        makeLabel("select output folder", toolPane, this.layout, this.gbc);
        this.gbc.gridx = 0;
        this.gbc.gridy = 8;
        this.gbc.weightx = 3.0D;
        this.gbc.weighty = 4.0D;
        this.gbc.gridwidth = 3;
        this.gbc.gridheight = 1;
        text4 = makeTextField(out, toolPane, this.layout, this.gbc);
        this.gbc.insets.set(10, -12, 10, 10);
        this.gbc.gridx = 3;
        this.gbc.gridy = 8;
        this.gbc.weightx = 1.0D;
        this.gbc.weighty = 4.0D;
        this.gbc.gridwidth = 1;
        this.gbc.gridheight = 1;
        JButton sel4 = makeButton("...", toolPane, this.layout, this.gbc);
        this.gbc.insets.set(10, 10, 10, 10);
        this.gbc.insets = new Insets(10, 10, 10, 10);
        this.gbc.gridx = 0;
        this.gbc.gridy = 9;
        this.gbc.weightx = 2.0D;
        this.gbc.weighty = 6.0D;
        this.gbc.gridwidth = 2;
        this.gbc.gridheight = 1;
        makeButton("execute", toolPane, this.layout, this.gbc);
        this.gbc.gridx = 2;
        this.gbc.gridy = 9;
        this.gbc.weightx = 2.0D;
        this.gbc.weighty = 6.0D;
        this.gbc.gridwidth = 2;
        this.gbc.gridheight = 1;
        makeButton("move", toolPane, this.layout, this.gbc);
        this.gbc.gridx = 4;
        this.gbc.gridy = 1;
        this.gbc.weightx = 4.0D;
        this.gbc.weighty = 1.0D;
        this.gbc.gridwidth = 3;
        this.gbc.gridheight = 7;
        textarea = makeTextArea("state: \n", this.layout, this.gbc);
        this.gbc.gridx = 4;
        this.gbc.gridy = 8;
        this.gbc.weightx = 2.0D;
        this.gbc.weighty = 1.0D;
        this.gbc.gridwidth = 1;
        this.gbc.gridheight = 1;
        cb = makeCheckBox("rename", this.layout, this.gbc);
        this.gbc.gridx = 5;
        this.gbc.gridy = 8;
        this.gbc.weightx = 2.0D;
        this.gbc.weighty = 1.0D;
        this.gbc.gridwidth = 2;
        this.gbc.gridheight = 1;
        text5 = makeTextField(name, toolPane, this.layout, this.gbc);
        this.gbc.gridx = 4;
        this.gbc.gridy = 9;
        this.gbc.weightx = 4.0D;
        this.gbc.weighty = 1.0D;
        this.gbc.gridwidth = 3;
        this.gbc.gridheight = 1;
        makeButton("quit", toolPane, this.layout, this.gbc);
        add(toolPane, "Center");
        this.fc = new JFileChooser(file);
        this.fc.setFileSelectionMode(1);
        sel1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                FF.this.setAlwaysOnTop(false);
                int value = FF.this.fc.showOpenDialog(null);
                if (value == 1) {
                    FF.this.setAlwaysOnTop(FF.top);
                    return;
                }
                FF.file = FF.this.fc.getSelectedFile();
                FF.text1.setText(FF.file.getAbsolutePath());
                FF.this.setAlwaysOnTop(FF.top);
            }
        });
        sel2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                FF.this.setAlwaysOnTop(false);
                int value = FF.this.fc.showOpenDialog(null);
                if (value == 1) {
                    FF.this.setAlwaysOnTop(FF.top);
                    return;
                }
                FF.file = FF.this.fc.getSelectedFile();
                FF.text2.setText(FF.file.getAbsolutePath());
                FF.this.setAlwaysOnTop(FF.top);
            }
        });
        sel3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                FF.this.setAlwaysOnTop(false);
                int value = FF.this.fc.showOpenDialog(null);
                if (value == 1) {
                    FF.this.setAlwaysOnTop(FF.top);
                    return;
                }
                FF.file = FF.this.fc.getSelectedFile();
                FF.text3.setText(FF.file.getAbsolutePath());
                FF.this.setAlwaysOnTop(FF.top);
            }
        });
        sel4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                FF.this.setAlwaysOnTop(false);
                int value = FF.this.fc.showOpenDialog(null);
                if (value == 1) {
                    FF.this.setAlwaysOnTop(FF.top);
                    return;
                }
                FF.file = FF.this.fc.getSelectedFile();
                FF.text4.setText(FF.file.getAbsolutePath());
                FF.this.setAlwaysOnTop(FF.top);
            }
        });
        setVisible(true);
    }

    public JCheckBox makeCheckBox(String choice, GridBagLayout layout, GridBagConstraints gbc) {
        JCheckBox checkBox;
        if (choice.equals("top")) {
            checkBox = new JCheckBox(choice, top);
        } else {
            checkBox = new JCheckBox(choice);
        }
        layout.setConstraints(checkBox, gbc);
        checkBox.addItemListener(this);
        checkBox.setFont(font);
        checkBox.setForeground(color3);
        toolPane.add(checkBox);
        return checkBox;
    }

    public void makeItem(String[] items, JMenu jMenu) {
        for (int i = 0; i < items.length; i++) {
            JMenuItem item = new JMenuItem(items[i]);
            item.addActionListener(this);
            item.setFont(font);
            item.setForeground(color3);
            jMenu.add(item);
        }
    }

    public JMenu makeJMenu(String name, char Mnemonic) {
        JMenu menu = new JMenu(name);
        menu.setMnemonic(Mnemonic);
        menu.setFont(font);
        menu.setForeground(color3);
        this.jmb.add(menu);
        return menu;
    }

    public JButton makeButton(String name, JPanel toolPane, GridBagLayout layout, GridBagConstraints gbc) {
        JButton button = new JButton(name);
        layout.setConstraints(button, gbc);
        button.addActionListener(this);
        button.setFont(font);
        button.setForeground(color3);
        toolPane.add(button);
        return button;
    }

    public JTextArea makeTextArea(String text, GridBagLayout layout, GridBagConstraints gbc) {
        ConsoleTextArea conarea = null;
        try {
            conarea = new ConsoleTextArea(text);
        } catch (IOException e) {
            System.err.println("can not create LoopedStream" + e);
            System.exit(1);
        }
        conarea.setFont(font2);
        conarea.setBackground(color2);
        conarea.setForeground(color);
        JScrollPane jsp = new JScrollPane(conarea);
        layout.setConstraints(jsp, gbc);
        toolPane.add(jsp);
        return conarea;
    }

    public JTextField makeTextField(String text, JPanel toolPane, GridBagLayout layout, GridBagConstraints gbc) {
        JTextField textField = new JTextField(text);
        layout.setConstraints(textField, gbc);
        textField.addActionListener(this);
        textField.setFont(font);
        textField.setForeground(color3);
        toolPane.add(textField);
        return textField;
    }

    public static JLabel makeLabel(String note, JPanel toolPane, GridBagLayout layout, GridBagConstraints gbc) {
        JLabel label = new JLabel(note);
        layout.setConstraints(label, gbc);
        label.setFont(font);
        label.setForeground(color3);
        toolPane.add(label);
        return label;
    }

    public static void main(String[] args) {
        new FF();
        while (true) {
            begin = text1.getText();
            bit = begin.split("\\\\")[(begin.split("\\\\")).length - 1];
            end = text2.getText();
            bat = text3.getText();
            out = text4.getText();
            name = cb.isSelected() ? text5.getText() : "";
        }
    }

    public void actionPerformed(ActionEvent arg0) {
        this.jtf.setText(arg0.getActionCommand());
        if (arg0.getActionCommand().equals("help")) {
            JFrame help = new JFrame("help");
            help.setSize(650, 350);
            help.setLocationRelativeTo((Component) null);
            JTextArea area = new JTextArea("~~~~~~~~~~~~~~\n Install ffmpeg first !\n\nComputer - Advanced system settings \nEnvironment Variables - PATH - Add : \n\\TOOL\\ffmpeg-4.2.2-win64-static\\bin;\n\n~~~~~~~~~~~~~~~Tips~~~~~~~~~~~~~~~~\nTips audio.m4s + video.m4s = out.mp4\nTipsaudio.m4s   video.m4s   video.bat \nTips Builds - Zeranoe FFmpeg  https://ffmpeg.zeranoe.com/builds/\nTips ffmpeg -i video.m4s -i audio.m4s -codec copy video.mp4\nTips/c 123 /m  1):  2):  3)\n");
            area.setFont(font);
            help.add(area);
            help.setVisible(true);
        }
        if (arg0.getActionCommand().equals("version")) {
            JFrame help = new JFrame("version");
            help.setSize(650, 310);
            help.setLocationRelativeTo((Component) null);
            JTextArea area = new JTextArea("~~~~~~~~~~~~~~\n from : https://www.bilibili.com/read/cv935685\n~~~~~~~~~~~~~~~\nName : BiliNote\nVersion : 1.0\nAuthor@chang09\n\n");
            area.setFont(font);
            help.add(area);
            help.setVisible(true);
        }
        if (arg0.getActionCommand().equals("font")) {
            MyFont mf = new MyFont();
            Thread th = new Thread(mf);
            th.start();
        }
        if (arg0.getActionCommand().equals("theme")) {
            MyTheme mt = new MyTheme();
            Thread th = new Thread(mt);
            th.start();
        }
        if (arg0.getActionCommand().equals("clear")) {
            ExecutorService ftdpool = Executors.newFixedThreadPool(2);
            Remove re = new Remove();
            Thread th = new Thread(re);
            Thread thread = new Thread(new Runnable() {
                public void run() {
                    while (true)
                        FF.textarea.setSelectionStart(FF.textarea.getText().length());
                }
            });
            ftdpool.execute(th);
            ftdpool.execute(thread);
        }
        if (arg0.getActionCommand().equals("quit")) {
            try {
                ReadWriter.write();
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.exit(0);
        }
        if (arg0.getActionCommand().equals("execute")) {
            Mixup mix = new Mixup();
            ExecutorService ftdpool = Executors.newFixedThreadPool(2);
            ftdpool.execute(mix);
            Thread thread = new Thread(new Runnable() {
                public void run() {
                    while (Mixup.count != 0)
                        FF.textarea.setSelectionStart(FF.textarea.getText().length());
                }
            });
            ftdpool.execute(thread);
            System.out.println("traversal parsing and creating...");
        }
        if (arg0.getActionCommand().equals("move")) {
            ExecutorService ftdpool = Executors.newFixedThreadPool(2);
            Move mo = new Move();
            Thread thread = new Thread(new Runnable() {
                public void run() {
                    while (Move.finish == 0)
                        FF.textarea.setSelectionStart(FF.textarea.getText().length());
                }
            });
            ftdpool.execute(mo);
            ftdpool.execute(thread);
            System.out.println("traversing and copy to output folder...");
        }
        if (arg0.getActionCommand().equals("output")) {
            File outPut = new File(text4.getText());
            if (!outPut.exists()) {
                System.out.println("check the output folder path !");
            } else {
                try {
                    Desktop.getDesktop().open(outPut);
                } catch (IOException iOException) {
                }
            }
        }
        if (arg0.getActionCommand().equals("bat")) {
            File batFolder = new File(text3.getText());
            if (!batFolder.exists()) {
                System.out.println("check the bat folder path !");
            } else {
                try {
                    Desktop.getDesktop().open(new File(text3.getText()));
                } catch (IOException iOException) {
                }
            }
        }
        if (arg0.getActionCommand().equals("end")) {
            File endFolder = new File(text2.getText());
            if (!endFolder.exists()) {
                System.out.println("check the end folder path !");
            } else {
                try {
                    Desktop.getDesktop().open(new File(text2.getText()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (arg0.getActionCommand().equals("begin")) {
            File beginFolder = new File(text1.getText());
            if (!beginFolder.exists()) {
                System.out.println("check the begin folder path !");
            } else {
                try {
                    Desktop.getDesktop().open(new File(text1.getText()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
        public void itemStateChanged (ItemEvent arg0){
            JCheckBox box = (JCheckBox) arg0.getSource();
            if (box.equals(cb))
                if (cb.isSelected()) {
                    this.jtf.setText("video name  : name + number");
                } else {
                    this.jtf.setText("video name : number only");
                }
            if (box.equals(ck))
                if (ck.isSelected()) {
                    top = true;
                    setAlwaysOnTop(top);
                } else {
                    top = false;
                    setAlwaysOnTop(top);
                }
        }
    }


