package com.note.tool;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class Move extends Thread {
    static int finish = 0;

    public void run() {
        Scanner sc = new Scanner(System.in);
        System.out.println("got "+ FF.bit + " begin folder : "+ FF.begin);
        String str = FF.begin;
        String[] ss = str.split("\\\\");
        FF.bit = ss[ss.length - 1];
        str = ss[ss.length - 2];
        String prestr = str.substring(0, 2);
        str = str.substring(2, str.length());
        int n = Integer.parseInt(str);
        System.out.println("got "+ FF.bit + " end folder : "+ FF.end);
        str = FF.end;
        ss = str.split("\\\\");
        FF.bit = ss[ss.length - 1];
        str = ss[ss.length - 2];
        str = str.substring(2, str.length());
        int m = Integer.parseInt(str);
        int count = 0;
        System.out.println("got output folder : "+ FF.out);
                String string = FF.out;
        str = "";
        for (int i = n; i <= m; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < ss.length; j++) {
                if (j == ss.length - 2) {
                    sb.append(prestr);
                    sb.append(String.valueOf(i));
                    sb.append("\\");
                } else {
                    sb.append(ss[j]);
                    sb.append("\\");
                }
            }
            str = String.valueOf(sb);
            File f = new File(str);
            System.out.println("traversal parsing dirs : "+ i);
            try {
                if (f.exists()) {
                    System.out.println("have found : "+ f);
                            count++;
                    File ff = new File(str, "out.mp4");
                    double ffLen = (ff.length() / 1024L / 1024L);
                    File file = new File(string);
                    if (!file.exists())
                        file.mkdirs();
                    File fv = new File(file, FF.name + count + "out.mp4");
                    if (!fv.exists())
                        fv.createNewFile();
                    System.out.println("create empty : "+ fv);
                            FileOutputStream fos = new FileOutputStream(fv);
                    FileInputStream fis = new FileInputStream(ff);
                    BufferedInputStream bis = new BufferedInputStream(fis);
                    BufferedOutputStream bos = new BufferedOutputStream(fos);
                    byte[] bytes = new byte[102400];
                    int x = 0, y = 0;
                    while ((x = bis.read(bytes)) != -1) {
                        bos.write(bytes, 0, x);
                        System.out.println("copy progress : " + String.format("%.2f", new Object[] { Double.valueOf(++y * 100.0D / 1024.0D) }) + "  / " + String.format("%.0f", new Object[] { Double.valueOf(ffLen) }) + " MB ,  current : "+ FF.name + count + ".mp4");
                    }
                    fis.close();
                    fos.close();
                    System.out.println("- Video file : " + FF.name + count + ".mp4 file copy completed \n- Temp file : "+ ff);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (count == 0) {
            System.out.println("- Copy failed !");
                    finish = 1;
        } else {
            System.out.println("- Move done , files count : " + count + " \n- How to delete temp files : setting - clear" );
                    finish = 1;
        }
    }
}
