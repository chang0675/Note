package com.note.tool;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MyTheme implements Runnable, ItemListener, ActionListener {
    JButton app;

    JButton def;

    JTextField text1;

    JTextField text2;

    JTextField text3;

    JTextField text4;

    JTextField text5;

    JTextField text6;

    public void run() {
        JFrame themeF = new JFrame("theme");
                themeF.setSize(800, 600);
        themeF.setLocationRelativeTo((Component)null);
        GridBagLayout layout = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = 1;
        gbc.insets = new Insets(28, 25, 28, 55);
        JPanel panel = new JPanel();
        panel.setLayout(layout);
        JPanel panel2 = new JPanel();
        panel2.setLayout(layout);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1.0D;
        gbc.weighty = 1.0D;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        this.def = makeButton("default and quit", panel2, layout, gbc);
                gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 1.0D;
        gbc.weighty = 1.0D;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        this.app = makeButton("save and quit", panel2, layout, gbc);
        try {
            ReadWriter.read();
        } catch (Exception e) {
            e.printStackTrace();
        }
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 1.0D;
        gbc.weighty = 1.0D;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        FF.makeLabel("interface width", panel, layout, gbc).setHorizontalAlignment(4);
                gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 1.0D;
        gbc.weighty = 1.0D;
        gbc.gridwidth = 3;
        gbc.gridheight = 1;
        this.text1 = makeTextField(String.valueOf(FF.width), panel, layout, gbc);
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.weightx = 1.0D;
        gbc.weighty = 1.0D;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        FF.makeLabel("interface heigth", panel, layout, gbc).setHorizontalAlignment(4);
                gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.weightx = 1.0D;
        gbc.weighty = 1.0D;
        gbc.gridwidth = 3;
        gbc.gridheight = 1;
        this.text2 = makeTextField(String.valueOf(FF.height), panel, layout, gbc);
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.weightx = 1.0D;
        gbc.weighty = 1.0D;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        FF.makeLabel("status discribe color", panel, layout, gbc).setHorizontalAlignment(4);
                gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.weightx = 1.0D;
        gbc.weighty = 1.0D;
        gbc.gridwidth = 3;
        gbc.gridheight = 1;
        this.text3 = makeTextField(Color2String(FF.color), panel, layout, gbc);
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.weightx = 1.0D;
        gbc.weighty = 1.0D;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        FF.makeLabel("status background color", panel, layout, gbc).setHorizontalAlignment(4);
                gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.weightx = 1.0D;
        gbc.weighty = 1.0D;
        gbc.gridwidth = 3;
        gbc.gridheight = 1;
        this.text4 = makeTextField(Color2String(FF.color2), panel, layout, gbc);
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.weightx = 1.0D;
        gbc.weighty = 1.0D;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        FF.makeLabel("interface discribe color", panel, layout, gbc).setHorizontalAlignment(4);
                gbc.gridx = 1;
        gbc.gridy = 5;
        gbc.weightx = 1.0D;
        gbc.weighty = 1.0D;
        gbc.gridwidth = 3;
        gbc.gridheight = 1;
        this.text5 = makeTextField(Color2String(FF.color3), panel, layout, gbc);
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.weightx = 1.0D;
        gbc.weighty = 1.0D;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        FF.makeLabel("16 | 64 | 80 | 128", panel, layout, gbc).setHorizontalAlignment(4);
                gbc.gridx = 1;
        gbc.gridy = 6;
        gbc.weightx = 1.0D;
        gbc.weighty = 1.0D;
        gbc.gridwidth = 3;
        gbc.gridheight = 1;
        this.text6 = makeTextField(FF.bit, panel, layout, gbc);
        themeF.add(panel, "Center");
        themeF.add(panel2, "South");
        themeF.setVisible(true);
        while (true) {
            try {
                while (true) {
                    FF.width = Integer.parseInt(this.text1.getText());
                    Thread.sleep(100L);
                    FF.height = Integer.parseInt(this.text2.getText());
                    Thread.sleep(100L);
                    FF.color = String2Color(this.text3.getText());
                    Thread.sleep(100L);
                    FF.color2 = String2Color(this.text4.getText());
                    Thread.sleep(100L);
                    FF.color3 = String2Color(this.text5.getText());
                    Thread.sleep(100L);
                    FF.bit = this.text6.getText();
                }
//                break;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public JButton makeButton(String name, JPanel toolPane, GridBagLayout layout, GridBagConstraints gbc) {
        JButton button = new JButton(name);
        layout.setConstraints(button, gbc);
        button.addActionListener(this);
        button.setSize(200, 100);
        button.setFont(FF.font);
        button.setForeground(FF.color3);
        toolPane.add(button);
        return button;
    }

    public JTextField makeTextField(String text, JPanel toolPane, GridBagLayout layout, GridBagConstraints gbc) {
        JTextField textField = new JTextField(text);
        layout.setConstraints(textField, gbc);
        textField.setFont(FF.font);
        textField.setForeground(FF.color3);
        toolPane.add(textField);
        return textField;
    }

    public void actionPerformed(ActionEvent arg0) {
        if (arg0.getActionCommand().equals("save and quit")) {
        try {
            ReadWriter.write();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
    if (arg0.getActionCommand().equals("default and quit")) {
                                       File file = new File("../Note/src/com/note/tool","info.txt");
      if (file.exists())
              try {
        file.delete();
    } catch (Exception e) {
        e.printStackTrace();
    }
      try {
        ReadWriter.read();
    } catch (Exception e) {
        e.printStackTrace();
    }
      System.exit(0);
}
  }

public static String Color2String(Color color) {
        String R = Integer.toHexString(color.getRed());
        R = (R.length() < 2) ? ('0' + R) : R;
        String G = Integer.toHexString(color.getGreen());
        G = (G.length() < 2) ? ('0' + G) : G;
        String B = Integer.toHexString(color.getBlue());
        B = (B.length() < 2) ? ('0' + B) : B;
        return '#' + R + G + B;
        }

public static Color String2Color(String str) {
        int i = 0;
        if (str.length() == 7) {
        i = Integer.parseInt(str.substring(1, 7), 16);
        } else {
        return FF.color;
        }
        return new Color(i);
        }

public void itemStateChanged(ItemEvent e) {}
        }
