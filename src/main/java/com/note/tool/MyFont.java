package com.note.tool;

import java.awt.Component;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class MyFont implements Runnable, ItemListener, ActionListener {
    JButton app = new JButton();

    JButton def = new JButton();

    JComboBox fontType = new JComboBox();

    JComboBox fontStyle = new JComboBox();

    JComboBox fontSize = new JComboBox();

    Font font;

    JTextArea area;

    public void run() {
        JFrame fontF = new JFrame("font");
                fontF.setSize(450, 350);
        fontF.setLocationRelativeTo((Component)null);
        JPanel panel = new JPanel();
        JPanel panel2 = new JPanel();
        String[] fontName = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
        for (int i = 0; i < fontName.length; i++) {
            this.fontType.addItem(fontName[i]);
        }
        this.fontType.setSelectedIndex(0);
        String[] style = { "common", "bold", "italic"};
        for (int j = 0; j < style.length; j++) {
            this.fontStyle.addItem(style[j]);
        }
        this.fontStyle.setSelectedIndex(0);
        Integer[] size = {
                5, 6, 7, 8, 9, 10, 11, 12, 14, 16,
                18, 20, 22, 24, 26, 28, 36, 48, 72 };
        for (int k = 0; k < size.length; k++) {
            this.fontSize.addItem(Integer.valueOf(size[k]));
        }
        this.fontSize.setSelectedIndex(0);
        GridBagLayout layout = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = 1;
        gbc.insets = new Insets(10, 10, 10, 10);
        panel.add(new JLabel("font"));
                panel.add(this.fontType);
        panel.add(new JLabel("type"));
                panel.add(this.fontStyle);
        panel.add(new JLabel("size"));
                panel.add(this.fontSize);
        this.area = new JTextArea("change to :\n 测试字体 \nText for Testing!");
        fontF.add(panel, "North");
        fontF.add(this.area, "Center");
        panel2.setLayout(layout);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1.0D;
        gbc.weighty = 1.0D;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        this.def = makeButton("default and quit", panel2, layout, gbc);
                gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 1.0D;
        gbc.weighty = 1.0D;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        this.app = makeButton("save and quit", panel2, layout, gbc);
                fontF.add(panel2, "South");
        fontF.setVisible(true);
        this.fontType.addItemListener(this);
        this.fontStyle.addItemListener(this);
        this.fontSize.addItemListener(this);
        this.app.addActionListener(this);
        this.def.addActionListener(this);
  }

        public JButton makeButton(String name, JPanel toolPane, GridBagLayout layout, GridBagConstraints gbc) {
            JButton button = new JButton(name);
            layout.setConstraints(button, gbc);
            button.addActionListener(this);
            button.setFont(FF.font);
            button.setForeground(FF.color3);
            toolPane.add(button);
            return button;
        }

        public void itemStateChanged(ItemEvent e) {
            int[] styleName = { 0, 1, 2 };
            String name = (String)this.fontType.getSelectedItem();
            int style1 = styleName[this.fontStyle.getSelectedIndex()];
            int size1 = ((Integer)this.fontSize.getSelectedItem()).intValue();
            this.font = new Font(name, style1, size1);
            this.area.setFont(this.font);
        }

        public void actionPerformed(ActionEvent arg0) {
            if (arg0.getActionCommand().equals("save and quit")) {
                    FF.font = this.font;
            try {
                ReadWriter.write();
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.exit(0);
        }
        if (arg0.getActionCommand().equals("default and quit")) {
                File file = new File("../Note/src/com/note/tool","info.txt");
        if (file.exists())
            try {
                file.delete();
            } catch (Exception e) {
                e.printStackTrace();
            }
        try {
            ReadWriter.read();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
}
}
