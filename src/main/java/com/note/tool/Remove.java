package com.note.tool;

import java.io.File;

public class Remove implements Runnable {
    public void run() {
        System.out.println("got "+ FF.bit + " begin folder : "+ FF.begin);
        String str = FF.begin;
        String[] ss = str.split("\\\\");
        FF.bit = ss[ss.length - 1];
        str = ss[ss.length - 2];
        String prestr = str.substring(0, 2);
        str = str.substring(2, str.length());
        int n = Integer.parseInt(str);
        System.out.println("got "+ FF.bit + " end folder : "+ FF.end);
        str = FF.end;
        ss = str.split("\\\\");
        FF.bit = ss[ss.length - 1];
        str = ss[ss.length - 2];
        str = str.substring(2, str.length());
        int m = Integer.parseInt(str);
        int count = 0;
        str = "";
        for (int i = n; i <= m; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < ss.length; j++) {
                if (j == ss.length - 2) {
                    sb.append(prestr);
                    sb.append(String.valueOf(i));
                    sb.append("\\");
                } else {
                    sb.append(ss[j]);
                    sb.append("\\");
                }
            }
            str = String.valueOf(sb);
            File f = new File(str);
            System.out.println("traversal parsing dirs : "+ i);
            if (f.exists()) {
                System.out.println("have found : "+ f);
                 File ff = new File(str, "out.mp4");
                 if (ff.exists()){
                 count++;
                 }
                ff.delete();
                System.out.println("+ count + "+ ff);
            }
            if (f.exists()) {
                File file = null;
                try {
                    file = new File(str, "Video.bat");
                    if (file.exists())
                        file.delete();
                    System.out.println("have deleted bat file : "+ file);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (count == 0) {
            System.out.println("delete failed , check path !");
        } else {
            System.out.println("- Have deleted " + count + " folder ,\n- With .mp4 and .bat files !");
        }
    }
}
