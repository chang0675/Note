package com.note.tool;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class ReadWriter {
    static ArrayList<Object> arr = new ArrayList();

    public static void write() throws Exception {
        File file = new File(".", "info.txt");
        if (!file.exists())
            file.createNewFile();
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        arr = new ArrayList();
        arr.add(Integer.valueOf(FF.width));
        arr.add(Integer.valueOf(FF.height));
        arr.add(FF.font);
        arr.add(FF.font2);
        arr.add(FF.color);
        arr.add(FF.color2);
        arr.add(FF.color3);
        arr.add(FF.bit);
        arr.add(FF.begin);
        arr.add(FF.end);
        arr.add(FF.bat);
        arr.add(FF.out);
        arr.add(FF.name);
        arr.add(FF.file);
        arr.add(Boolean.valueOf(FF.top));
        oos.writeObject(arr);
        oos.close();
        for (Object o : arr)
            System.out.println(o);
    }

    public static void read() throws Exception {
        File file = new File(".", "info.txt");
        if (!file.exists())
            file.createNewFile();
        FileInputStream fis = new FileInputStream(file);
        if (fis.available() > 0) {
            ObjectInputStream ois = new ObjectInputStream(fis);
            arr = (ArrayList<Object>)ois.readObject();
            ois.close();
            FF.width = ((Integer)arr.get(0)).intValue();
            FF.height = ((Integer)arr.get(1)).intValue();
            FF.font = (Font)arr.get(2);
            FF.font2 = (Font)arr.get(3);
            FF.color = (Color)arr.get(4);
            FF.color2 = (Color)arr.get(5);
            FF.color3 = (Color)arr.get(6);
            FF.bit = (String)arr.get(7);
            FF.begin = (String)arr.get(8);
            FF.end = (String)arr.get(9);
            FF.bat = (String)arr.get(10);
            FF.out = (String)arr.get(11);
            FF.name = (String)arr.get(12);
            FF.file = (File)arr.get(13);
            FF.top = ((Boolean)arr.get(14)).booleanValue();
            for (Object o : arr)
                System.out.println(o);
        } else {
            FF.width = 1200;
            FF.height = 800;
            FF.font = new Font("MicroSoft YaHei", 1, 16);
            FF.font2 = new Font("MicroSoft YaHei", 0, 12);
            FF.color = Color.green;
            FF.color2 = Color.black;
            FF.color3 = Color.black;
            FF.bit = "64";
            FF.begin = "C:\\Users\\liuyuan\\Desktop\\800229048\\c_252565201\\16";
            FF.end = "C:\\Users\\liuyuan\\Desktop\\800229048\\c_252609340\\80";
            FF.bat = "../Note/src/com/note/tool";
            FF.out = "F:\\FFOutput\\New";
            FF.name = "";
            FF.file = new File(".");
            FF.top = false;
            write();
        }
    }
}
