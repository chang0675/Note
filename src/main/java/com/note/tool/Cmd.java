package com.note.tool;

public class Cmd {
    public static int run_cmd(String strcmd) {
        Runtime rt = Runtime.getRuntime();
        Process ps = null;
        try {
            ps = rt.exec(strcmd);
            ps.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
        int i = ps.exitValue();
        ps.destroy();
        ps = null;
        if (i == 0) {
            System.out.println("start...");
            return 1;
        }
        System.out.println("failed...");
        return 0;
    }
}