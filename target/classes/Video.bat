@echo off
echo.
echo =======================================
echo 正在为批处理文件提升超级管理员权限。。。
echo =======================================
cd /d %~dp0
%1 start "" mshta vbscript:createobject("shell.application").shellexecute("""%~0""","::",,"runas",1)(window.close)&exit

@echo off
echo.
echo %username% 已获取超级管理员权限
echo.
echo.
echo.
:on
echo.   
echo.    官方网站：http://www.ffmpeg.org/download.html
echo.    环境变量：F:\TOOL\ffmpeg-4.2.2-win64-static\bin
echo.     
echo.    请确认已下载 ffmpeg 软件,并已配置好环境变量！
echo.            命令：  Builds - Zeranoe FFmpeg  https://ffmpeg.zeranoe.com/builds/
echo.    已将 audio.m4s video.m4s video.bat 放于同一文件夹内！
echo. 
echo    (1)合并音视频:  audio.m4s + video.m4s = and.mp4
echo.            命令：  ffmpeg -i video.m4s -i audio.m4s -codec copy video.mp4
echo    (2)拼接视频:
echo.   
echo    (3)退出；
echo. 
echo    正在使用FFmpeg进行拼接任务
echo    请勿关闭窗口：
echo.
cd /d %~dp0

goto A

:A
echo.
set /a count=1
setlocal enabledelayedexpansion
for %%c in (*.m4s) do (
	if !count!==1 ( 
		set MP3=%%c 
		set oldMP3=%%c
		rename "%%c" "MP3.mp4"
	)
	if !count!==2 ( 
		set MP4=%%c
		set oldMP4=%%c
		rename "%%c" "MP4.mp4"
	)
	set /a count=count+1
)
ffmpeg -i MP3.mp4 -i MP4.mp4 -vcodec copy -acodec copy out.mp4
rename "MP3.mp4" "%oldMP3%"
rename "MP4.mp4" "%oldMP4%"
echo 合并音视频成功!


 goto end

set name="%oldMP3:~0,-6%"
if %name%=="" (
	echo 文件名太短了!
	echo 合成结果为"out.mp4"
) else (
	rename "out.mp4" %name%.mp4
)
goto on

:B
echo.
echo 拼接视频还没有,可能后续更新？
goto on 


:end
echo 退出脚本

exit